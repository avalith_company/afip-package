<?php

namespace Avalith\AFIP;

use Avalith\AFIP\Services\Concept;
use Avalith\AFIP\Services\Voucher;
use Carbon\Carbon;
use SoapClient;

class Afip extends SoapClient
{
    /**
    * The WSAA instance.
    *
    * @var object
    */
    private $wsaa;

    /**
    * CUIT (11 digits without hyphens)
    *
    * @var number
    */
    private $cuit;

    /**
    * Create a new Afip instance.
    *
    * @param string $service
    *
    * @return void
    */
    public function __construct($service,$cuit)
    {
        if (config('afip.production') === true) {
            $this->wsfeUri = config('afip.wsfe_pro');
            $this->wsfeWsdlUri = config('afip.wsfe_wsdl_pro');
        } else {
            $this->wsfeUri = config('afip.wsfe_dev');
            $this->wsfeWsdlUri = config('afip.wsfe_wsdl_dev');
        }
        $this->cuit=$cuit;
        $this->wsaa = Wsaa::getInstance($service)->init();
        parent::SoapClient($this->wsfeWsdlUri, $this->WSFEoptions());
    }

    /**
    * Soap options.
    *
    * @return array
    */
    public function WSFEoptions()
    {
        return [
            'soap_version'   => SOAP_1_2,
            'location'       => $this->wsfeUri,
            'trace'          => 1,
            'exceptions'     => true,
            // 'stream_context' => stream_context_create([
            //   'ssl' => [
            //     'ciphers' => 'RC4-SHA',
            //     'verify_peer' => false,
            //     'verify_peer_name' => false,
            //     'allow_self_signed' => true
            //     ]
            // ]),
            'cache_wsdl'     => WSDL_CACHE_NONE
        ];
    }

    /**
    * Request to the AFIP webservice.
    *
    * @param string $method
    * @param array $options
    *
    * @return mixed
    */
    private function request($method, $options = [])
    {
        $options = $this->wsaa->getAuth($this->cuit) + $options;

        $results = $this->$method($options);

        $vars = get_object_vars(current($results));

        if (array_key_exists('Errors', $vars)) {
            return $vars['Errors'];
        } elseif (array_key_exists('ResultGet', $vars)) {
            return current($vars['ResultGet']);
        }

        return $vars;
    }

    /**
    * Dummy method for verification operation.
    *
    * @return array
    */
    public function serverStatus()
    {
        return $this->request('FEDummy');
    }

    /**
    * Retrieve the list of registered points of sale and their status.
    *
    * @return array
    */
    public function posNumbers()
    {
      $tmp = $this->request('FEParamGetPtosVenta');
      if (isset($tmp->Err)) return $tmp;
      if (!is_array($tmp)) {$temp[]=$tmp;} else {$temp=$tmp;}
      foreach ($temp as $key => $value) {
        if ($value->FchBaja!="NULL") {unset($temp[$key]);}
      }
      foreach ($temp as $key => $value) {
        $temp[$key]->A = $this->lastVoucher($value->Nro, 1)['CbteNro'];
        $temp[$key]->NDA = $this->lastVoucher($value->Nro, 2)['CbteNro'];
        $temp[$key]->NCA = $this->lastVoucher($value->Nro, 3)['CbteNro'];
        $temp[$key]->B = $this->lastVoucher($value->Nro, 6)['CbteNro'];
        $temp[$key]->NDB = $this->lastVoucher($value->Nro, 7)['CbteNro'];
        $temp[$key]->NCB = $this->lastVoucher($value->Nro, 8)['CbteNro'];
        $temp[$key]->C = $this->lastVoucher($value->Nro, 11)['CbteNro'];
        $temp[$key]->NDC = $this->lastVoucher($value->Nro, 12)['CbteNro'];
        $temp[$key]->NCC = $this->lastVoucher($value->Nro, 13)['CbteNro'];
        $temp[$key]->M = $this->lastVoucher($value->Nro, 51)['CbteNro'];
        $temp[$key]->NDM = $this->lastVoucher($value->Nro, 52)['CbteNro'];
        $temp[$key]->NCM = $this->lastVoucher($value->Nro, 53)['CbteNro'];
      }
      return $temp;
    }

    /**
    * Retrieve the list of registered points of sale and their status.
    *
    * @return array
    */
    public function pointsOfSale()
    {
      $tmp = $this->request('FEParamGetPtosVenta');
      if (isset($tmp->Err)) return $tmp;
      if (!is_array($tmp)) {$temp[]=$tmp;} else {$temp=$tmp;}
      foreach ($temp as $key => $value) {
        if ($value->FchBaja!="NULL") {unset($temp[$key]);}
      }
      return $temp;
    }

    /**
    * Retrieve the list of types of vouchers usable authorization service.
    *
    * @return array
    */
    public function voucherTypes()
    {
        return $this->request('FEParamGetTiposCbte');
    }

    /**
    * Retrieve the list of identifiers for the Concept field.
    *
    * @return array
    */
    public function conceptTypes()
    {
        return $this->request('FEParamGetTiposConcepto');
    }

    /**
    * Retrieve the list of types of documents used in authorization service.
    *
    * @return array
    */
    public function documentTypes()
    {
        return $this->request('FEParamGetTiposDoc');
    }

    /**
    * Retrieve the list of IVA rates used in authorization service..
    *
    * @return array
    */
    public function ivaTypes()
    {
        return $this->request('FEParamGetTiposIva');
    }

    /**
    * Retrieve the list of reusable coins authorization service.
    *
    * @return array
    */
    public function currencyRates()
    {
        return $this->request('FEParamGetTiposMonedas');
    }

    /**
    * Retrieve the list of the different taxes that can be used in the authorization service.
    *
    * @return array
    */
    public function taxTypes()
    {
        return $this->request('FEParamGetTiposTributos');
    }

    /**
    * Return the last proof approved for the type of voucher.
    *
    * @return array
    */
    public function findCAE($pointSale, $type, $number)
    {
        return $this->request('FECompConsultar', [ 'FeCompConsReq' =>[
            'CbteTipo' => $type,
            'CbteNro' => $number,
            'PtoVta' => $pointSale
        ]]);

    }

    /**
    * Return the last proof approved for the type of voucher.
    *
    * @return array
    */
    public function lastVoucher($pointSale, $type)
    {
        return $this->request('FECompUltimoAutorizado', [
            'PtoVta' => $pointSale,
            'CbteTipo' => $type
        ]);
    }

    public function requestCAE($request)
    {
        return $this->request('FECAESolicitar', $request);
    }

    /**
    * Add a new voucher;
    *
    * @return Avalith\AFIP\Services\Voucher
    */
    public function voucher()
    {
        return new Voucher;
    }
}
